import animals.Animal;
import animals.interfaces.Voice;
import animals.model.*;
import food.Food;
import food.model.*;

public class Zoo {
    public static void main(String[] args) {


        Worker worker = new Worker();
        Pony pony = new Pony("Pony","Igogo");
        Elephant elephant = new Elephant("Dendy","Thruuu");
        Duck duck = new Duck("Villy","Kua");
        Fish fish = new Fish("Voopy");
        Racoon racoon = new Racoon("Toby","Whii");
        Tiger tiger = new Tiger("Khan","Grrr");

        Food carrot = new Carrot();
        Food corn = new Corn();
        Food cucumber = new Cucumber();
        Food meatBall = new MeatBall();
        Food meatChips = new MeatChips();
        Food rawMeat = new RawMeat();

        worker.feed(carrot, tiger);
        worker.feed(corn, racoon);
        worker.getVoice(duck);
        worker.getVoice(pony);
        worker.feed(cucumber, fish);
        worker.getVoice(elephant);
        worker.feed(meatBall, racoon);
        worker.feed(rawMeat, tiger);
        worker.feed(meatChips, pony);
        worker.getVoice(tiger);

        Pond pond = new Pond();
        pond.swimmers();

        System.out.println(tiger.voice());
        System.out.println(elephant.voice());

    }

}
