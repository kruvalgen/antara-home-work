package animals;

import food.Food;

public abstract class Animal {

    public String name;


    public String getName() {
        return name;
    }


    public abstract void eat(Food food);

}
