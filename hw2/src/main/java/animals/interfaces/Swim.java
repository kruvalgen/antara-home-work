package animals.interfaces;

public interface Swim {

    public String swim();
}
