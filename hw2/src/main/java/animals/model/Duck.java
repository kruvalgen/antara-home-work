package animals.model;

import food.Food;
import animals.Herbivore;
import animals.interfaces.Fly;
import animals.interfaces.Run;
import animals.interfaces.Swim;
import animals.interfaces.Voice;

public class Duck extends Herbivore implements Voice, Run, Fly, Swim {


    private String voice;


    public Duck(String name, String voice) {
        this.name = name;
        this.voice = voice;
    }


    @Override
    public void fly() {
        System.out.println("Duck flying very good");
    }

    @Override
    public void run() {
        System.out.println("Duck runs very funny");

    }

    @Override
    public String swim() {
        return "Duck swims best of all";
    }

    @Override
    public String voice() {
        return voice;
    }
}
