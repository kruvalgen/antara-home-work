package animals.model;

import animals.Carnivorous;
import animals.interfaces.Run;
import animals.interfaces.Swim;
import animals.interfaces.Voice;
import food.Food;

public class Elephant extends Carnivorous implements Voice, Run, Swim {


    private String voice;


    public Elephant(String name, String voice) {
        this.name = name;
        this.voice = voice;
    }


    @Override
    public String voice() {
        return voice;
    }

    @Override
    public void run() {

    }

    @Override
    public String swim() {
        return "Elephant swimmer";

    }
}
