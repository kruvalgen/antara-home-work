package animals.model;

import food.Food;
import animals.Herbivore;
import animals.interfaces.Swim;

public class Fish extends Herbivore implements Swim {

    public Fish() {
    }

    public Fish(String name) {
        this.name = name;
    }


    @Override
    public String swim() {
        return "I am fish I am swim";
    }
}