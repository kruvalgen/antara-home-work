package animals.model;

import animals.Animal;
import animals.Herbivore;
import animals.interfaces.Run;
import animals.interfaces.Voice;
import food.Food;


public class Pony extends Herbivore implements Voice, Run {


    private String voice;


    public Pony(String name, String voice) {
        this.name = name;
        this.voice = voice;
    }


    @Override
    public String voice() {
        return voice;
    }

    @Override
    public void run() {

    }
}
