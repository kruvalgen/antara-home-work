package animals.model;

import animals.Carnivorous;
import animals.interfaces.Run;
import animals.interfaces.Swim;
import animals.interfaces.Voice;
import food.Food;

public class Racoon extends Carnivorous implements Swim, Voice, Run {

    private String voice;


    public Racoon() {
    }

    public Racoon(String name, String voice) {
        this.name = name;
        this.voice = voice;
    }


    @Override
    public void run() {

    }

    @Override
    public String swim() {
        return "I am swimmer racoon";

    }

    @Override
    public String voice() {
        return voice;
    }
}
